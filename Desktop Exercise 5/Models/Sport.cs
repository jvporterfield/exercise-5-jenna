using Desktop_Exercise_5.Interfaces;
using Desktop_Exercise_5.Utility;
using System;

namespace Desktop_Exercise_5.Models
{
  public class Sport : ISport
  {
    private string _sportName;
    private Lookups.SportCategory _sportCategory;
    
    public Sport(string name, int players, Lookups.SportCategory category)
    {
      _sportName = name;
      _sportCategory = category;
      NumberOfPlayers = players;
    }

    public Lookups.SportCategory SportCategory
    {
      get { return _sportCategory; }
    }

    public int NumberOfPlayers { get; set; }

    public string SportName
    {
      get { return _sportName; }
    }

    public void WriteSportProperties()
    {
      Console.WriteLine("* Sport Properties *\n");
      Console.WriteLine("Sport Name:{0}\nCategory: {1}\nNo. of Players: {2}\nCourt Dimensions: {3}ft x {4}ft \n\n", SportName, SportCategory, NumberOfPlayers, Length, Width);
    }

    public decimal Length { get; set; }

    public decimal Width { get; set; }
  }
}