﻿using Desktop_Exercise_5.Models;
using Desktop_Exercise_5.Utility;
using System;

namespace Desktop_Exercise_5
{
  class Program
  {
    static void Main(string[] args)
    {
      Sport sc1 = new Sport("Volleyball", 6, Lookups.SportCategory.Team);
      sc1.Length = 60;
      sc1.Width = 35;

      Sport sc2 = new Sport("Bowling", 1, Lookups.SportCategory.Single);
      sc2.Length = 30;
      sc2.Width = 5;

      Sport sc3 = new Sport("Foosball", 1, Lookups.SportCategory.Unknown);
      sc3.Length = 6;
      sc3.Width = 3;

      sc1.WriteSportProperties();
      sc2.WriteSportProperties();
      sc3.WriteSportProperties();
      Console.Read();
    }

  }
}
